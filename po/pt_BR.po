# Brazilian Portuguese translation for desktop-icons.
# Copyright (C) 2019 desktop-icons's COPYRIGHT HOLDER
# This file is distributed under the same license as the desktop-icons package.
# Enrico Nicoletto <liverig@gmail.com>, 2018.
# Rafael Fontenelle <rafaelff@gnome.org>, 2018-2019.
# Fernando Alves <fernandoalvess617@gmail.com>, 2020.
# Diolinux <blogdiolinux@gmail.com>, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: desktop-icons master\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-10-23 22:38+0200\n"
"PO-Revision-Date: 2019-04-29 17:35-0300\n"
"Last-Translator: Rafael Fontenelle <rafaelff@gnome.org>\n"
"Language-Team: Brazilian Portuguese <gnome-pt_br-list@gnome.org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1)\n"
"X-Generator: Gtranslator 3.32.0\n"

#: app/askRenamePopup.js:46
#, fuzzy
msgid "Folder name"
msgstr "Nome da nova pasta"

#: app/askRenamePopup.js:46
#, fuzzy
msgid "File name"
msgstr "Renomear…"

#: app/askRenamePopup.js:54 app/autoAr.js:305 app/desktopManager.js:977
msgid "OK"
msgstr "OK"

#: app/askRenamePopup.js:54
#, fuzzy
msgid "Rename"
msgstr "Renomear…"

#: app/autoAr.js:88
msgid "AutoAr is not installed"
msgstr ""

#: app/autoAr.js:89
msgid ""
"To be able to work with compressed files, install file-roller and/or gir-1.2-"
"gnomeAutoAr"
msgstr ""

#: app/autoAr.js:224
#, fuzzy
msgid "Extracting files"
msgstr "Extrair aqui"

#: app/autoAr.js:241
#, fuzzy
msgid "Compressing files"
msgstr "Comprimir {0} arquivo"

#: app/autoAr.js:297 app/autoAr.js:636 app/desktopManager.js:979
#: app/fileItemMenu.js:460
msgid "Cancel"
msgstr "Cancelar"

#: app/autoAr.js:318 app/autoAr.js:619
msgid "Enter a password here"
msgstr ""

#: app/autoAr.js:359
msgid "Removing partial file '${outputFile}'"
msgstr ""

#: app/autoAr.js:378
msgid "Creating destination folder"
msgstr ""

#: app/autoAr.js:410
msgid "Extracting files into '${outputPath}'"
msgstr ""

#: app/autoAr.js:442
#, fuzzy
msgid "Extraction completed"
msgstr "Extrair aqui"

#: app/autoAr.js:443
msgid "Extracting '${fullPathFile}' has been completed."
msgstr ""

#: app/autoAr.js:449
#, fuzzy
msgid "Extraction cancelled"
msgstr "Extrair aqui"

#: app/autoAr.js:450
msgid "Extracting '${fullPathFile}' has been cancelled by the user."
msgstr ""

#: app/autoAr.js:460
msgid "Passphrase required for ${filename}"
msgstr ""

#: app/autoAr.js:463
msgid "Error during extraction"
msgstr ""

#: app/autoAr.js:492
msgid "Compressing files into '${outputFile}'"
msgstr ""

#: app/autoAr.js:505
msgid "Compression completed"
msgstr ""

#: app/autoAr.js:506
msgid "Compressing files into '${outputFile}' has been completed."
msgstr ""

#: app/autoAr.js:510 app/autoAr.js:517
msgid "Cancelled compression"
msgstr ""

#: app/autoAr.js:511
msgid "The output file '${outputFile}' already exists."
msgstr ""

#: app/autoAr.js:518
msgid "Compressing files into '${outputFile}' has been cancelled by the user."
msgstr ""

#: app/autoAr.js:521
msgid "Error during compression"
msgstr ""

#: app/autoAr.js:554
#, fuzzy
msgid "Create archive"
msgstr "Criar"

#: app/autoAr.js:579
#, fuzzy
msgid "Archive name"
msgstr "Renomear…"

#: app/autoAr.js:614
msgid "Password"
msgstr ""

#: app/autoAr.js:633
msgid "Create"
msgstr "Criar"

#: app/autoAr.js:708
msgid "Compatible with all operating systems."
msgstr ""

#: app/autoAr.js:714
msgid "Password protected .zip, must be installed on Windows and Mac."
msgstr ""

#: app/autoAr.js:720
msgid "Smaller archives but Linux and Mac only."
msgstr ""

#: app/autoAr.js:726
msgid "Smaller archives but must be installed on Windows and Mac."
msgstr ""

#: app/dbusUtils.js:68
msgid "\"${programName}\" is needed for Desktop Icons"
msgstr ""

#: app/dbusUtils.js:69
msgid ""
"For this functionality to work in Desktop Icons, you must install "
"\"${programName}\" in your system."
msgstr ""

#: app/desktopIconsUtil.js:136
msgid "Command not found"
msgstr "Comando não encontrado"

#: app/desktopManager.js:257
msgid "Nautilus File Manager not found"
msgstr "Gerenciador de arquivos Nautilus não encontrado"

#: app/desktopManager.js:258
msgid "The Nautilus File Manager is mandatory to work with Desktop Icons NG."
msgstr ""
"O gerenciador de arquivos Nautilus é um requisito para a extensão Desktop "
"Icons NG funcionar."

#: app/desktopManager.js:939
msgid "Clear Current Selection before New Search"
msgstr ""

#: app/desktopManager.js:981
msgid "Find Files on Desktop"
msgstr ""

#: app/desktopManager.js:1047 app/desktopManager.js:1750
msgid "New Folder"
msgstr "Nova pasta"

#: app/desktopManager.js:1051
msgid "New Document"
msgstr "Novo documento"

#: app/desktopManager.js:1056
msgid "Paste"
msgstr "Colar"

#: app/desktopManager.js:1060
msgid "Undo"
msgstr "Desfazer"

#: app/desktopManager.js:1064
msgid "Redo"
msgstr "Refazer"

#: app/desktopManager.js:1070
#, fuzzy
msgid "Select All"
msgstr "Selecionar todos"

#: app/desktopManager.js:1078
msgid "Show Desktop in Files"
msgstr "Mostrar a área de trabalho nos Arquivos"

#: app/desktopManager.js:1082 app/fileItemMenu.js:358
msgid "Open in Terminal"
msgstr "Abrir no terminal"

#: app/desktopManager.js:1088
msgid "Change Background…"
msgstr "Alterar plano de fundo…"

#: app/desktopManager.js:1099
#, fuzzy
msgid "Desktop Icons Settings"
msgstr "Configurações dos ícones no Desktop"

#: app/desktopManager.js:1103
msgid "Display Settings"
msgstr "Configurações de exibição"

#: app/desktopManager.js:1763
#, fuzzy
msgid "Folder Creation Failed"
msgstr "Nome da nova pasta"

#: app/desktopManager.js:1764
#, fuzzy
msgid "Error while trying to create a Folder"
msgstr "Erro ao excluir os arquivos"

#: app/desktopManager.js:1800
msgid "Template Creation Failed"
msgstr ""

#: app/desktopManager.js:1801
msgid "Error while trying to create a Document"
msgstr ""

#: app/desktopManager.js:1809
msgid "Arrange Icons"
msgstr "Ordenar Ícones"

#: app/desktopManager.js:1813
msgid "Arrange By..."
msgstr "Ordenar por..."

#: app/desktopManager.js:1822
msgid "Keep Arranged..."
msgstr "Manter Ordenado..."

#: app/desktopManager.js:1826
msgid "Keep Stacked by type..."
msgstr ""

#: app/desktopManager.js:1831
msgid "Sort Home/Drives/Trash..."
msgstr "Ordenar Home/Drives/Lixeira..."

#: app/desktopManager.js:1837
msgid "Sort by Name"
msgstr "Ordenar por Nome"

#: app/desktopManager.js:1839
msgid "Sort by Name Descending"
msgstr "Ordenar por Nome de forma descendente"

#: app/desktopManager.js:1842
msgid "Sort by Modified Time"
msgstr "Ordenar por data de modificação"

#: app/desktopManager.js:1845
msgid "Sort by Type"
msgstr "Ordenar por Tipo"

#: app/desktopManager.js:1848
msgid "Sort by Size"
msgstr "Ordenar por Tamanho"

#. TRANSLATORS: "Home" is the text that will be shown in the user's personal folder
#: app/fileItem.js:168
msgid "Home"
msgstr "Pasta pessoal"

#: app/fileItem.js:291
msgid "Broken Link"
msgstr ""

#: app/fileItem.js:292
msgid "Can not open this File because it is a Broken Symlink"
msgstr ""

#: app/fileItem.js:346
#, fuzzy
msgid "Broken Desktop File"
msgstr "Mostrar a área de trabalho nos Arquivos"

#: app/fileItem.js:347
msgid ""
"This .desktop file has errors or points to a program without permissions. It "
"can not be executed.\n"
"\n"
"\t<b>Edit the file to set the correct executable Program.</b>"
msgstr ""

#: app/fileItem.js:353
msgid "Invalid Permissions on Desktop File"
msgstr ""

#: app/fileItem.js:354
msgid ""
"This .desktop File has incorrect Permissions. Right Click to edit "
"Properties, then:\n"
msgstr ""

#: app/fileItem.js:356
msgid ""
"\n"
"<b>Set Permissions, in \"Others Access\", \"Read Only\" or \"None\"</b>"
msgstr ""

#: app/fileItem.js:359
msgid ""
"\n"
"<b>Enable option, \"Allow Executing File as a Program\"</b>"
msgstr ""

#: app/fileItem.js:367
msgid ""
"This .desktop file is not trusted, it can not be launched. To enable "
"launching, right-click, then:\n"
"\n"
"<b>Enable \"Allow Launching\"</b>"
msgstr ""

#: app/fileItemMenu.js:135
msgid "Open All..."
msgstr "Abrir todos..."

#: app/fileItemMenu.js:135
msgid "Open"
msgstr "Abrir"

#: app/fileItemMenu.js:151
msgid "Stack This Type"
msgstr ""

#: app/fileItemMenu.js:151
msgid "Unstack This Type"
msgstr ""

#: app/fileItemMenu.js:164
msgid "Scripts"
msgstr "Scripts"

#: app/fileItemMenu.js:170
#, fuzzy
msgid "Open All With Other Application..."
msgstr "Abrir com outro aplicativo"

#: app/fileItemMenu.js:170
msgid "Open With Other Application"
msgstr "Abrir com outro aplicativo"

#: app/fileItemMenu.js:176
msgid "Launch using Dedicated Graphics Card"
msgstr "Abrir usando placa de vídeo dedicada"

#: app/fileItemMenu.js:188
msgid "Run as a program"
msgstr ""

#: app/fileItemMenu.js:196
msgid "Cut"
msgstr "Recortar"

#: app/fileItemMenu.js:203
msgid "Copy"
msgstr "Copiar"

#: app/fileItemMenu.js:211
msgid "Rename…"
msgstr "Renomear…"

#: app/fileItemMenu.js:221
msgid "Move to Trash"
msgstr "Mover para a lixeira"

#: app/fileItemMenu.js:229
msgid "Delete permanently"
msgstr "Excluir permanentemente"

#: app/fileItemMenu.js:239
#, fuzzy
msgid "Don't Allow Launching"
msgstr "Não permitir iniciar"

#: app/fileItemMenu.js:239
msgid "Allow Launching"
msgstr "Permitir iniciar"

#: app/fileItemMenu.js:252
msgid "Empty Trash"
msgstr "Esvaziar lixeira"

#: app/fileItemMenu.js:265
msgid "Eject"
msgstr "Ejetar"

#: app/fileItemMenu.js:273
msgid "Unmount"
msgstr "Desmontar"

#: app/fileItemMenu.js:287 app/fileItemMenu.js:294
msgid "Extract Here"
msgstr "Extrair aqui"

#: app/fileItemMenu.js:301
msgid "Extract To..."
msgstr "Extrair para…"

#: app/fileItemMenu.js:310
msgid "Send to..."
msgstr "Enviar para..."

#: app/fileItemMenu.js:318
#, fuzzy
msgid "Compress {0} folder"
msgid_plural "Compress {0} folders"
msgstr[0] "Comprimir {0} arquivo"
msgstr[1] "Comprimir {0} arquivos"

#: app/fileItemMenu.js:325
msgid "Compress {0} file"
msgid_plural "Compress {0} files"
msgstr[0] "Comprimir {0} arquivo"
msgstr[1] "Comprimir {0} arquivos"

#: app/fileItemMenu.js:333
msgid "New Folder with {0} item"
msgid_plural "New Folder with {0} items"
msgstr[0] "Nova pasta com {0} item"
msgstr[1] "Nova pasta com {0} itens"

#: app/fileItemMenu.js:344
#, fuzzy
msgid "Common Properties"
msgstr "Propriedades"

#: app/fileItemMenu.js:344
msgid "Properties"
msgstr "Propriedades"

#: app/fileItemMenu.js:351
#, fuzzy
msgid "Show All in Files"
msgstr "Mostrar todos no gestor de arquivos"

#: app/fileItemMenu.js:351
msgid "Show in Files"
msgstr "Mostrar no gestor de arquivos"

#: app/fileItemMenu.js:435
#, fuzzy
msgid "No Extraction Folder"
msgstr "Extrair aqui"

#: app/fileItemMenu.js:436
msgid "Unable to extract File, extraction Folder Does not Exist"
msgstr ""

#: app/fileItemMenu.js:456
msgid "Select Extract Destination"
msgstr "Selecionar destino da extração"

#: app/fileItemMenu.js:461
#, fuzzy
msgid "Select"
msgstr "Selecionar todos"

#: app/fileItemMenu.js:502
msgid "Can not email a Directory"
msgstr "Não é possível enviar um diretório por email"

#: app/fileItemMenu.js:503
msgid "Selection includes a Directory, compress the directory to a file first."
msgstr ""
"Seleção inclui um diretório, comprima o diretório em um arquivo primeiro"

#: app/notifyX11UnderWayland.js:37
msgid "Desktop Icons NG is running under X11Wayland"
msgstr ""

#: app/notifyX11UnderWayland.js:38
msgid ""
"It seems that you have your system configured to force GTK to use X11. This "
"works, but it's suboptimal. You should check your system configuration to "
"fix this."
msgstr ""

#: app/notifyX11UnderWayland.js:39 app/showErrorPopup.js:39
msgid "Close"
msgstr "Fechar"

#: app/notifyX11UnderWayland.js:47
msgid "Don't show this message anymore."
msgstr ""

#: app/preferences.js:91
msgid "Settings"
msgstr "Configurações"

#: app/prefswindow.js:64
msgid "Size for the desktop icons"
msgstr "Tamanho para os ícones da área de trabalho"

#: app/prefswindow.js:64
msgid "Tiny"
msgstr "Muito pequeno"

#: app/prefswindow.js:64
msgid "Small"
msgstr "Pequeno"

#: app/prefswindow.js:64
msgid "Standard"
msgstr "Padrão"

#: app/prefswindow.js:64
msgid "Large"
msgstr "Grande"

#: app/prefswindow.js:65
msgid "Show the personal folder in the desktop"
msgstr "Mostrar a pasta pessoal na área de trabalho"

#: app/prefswindow.js:66
msgid "Show the trash icon in the desktop"
msgstr "Mostrar o ícone da lixeira na área de trabalho"

#: app/prefswindow.js:67 schemas/org.gnome.shell.extensions.ding.gschema.xml:45
#, fuzzy
msgid "Show external drives in the desktop"
msgstr "Mostrar dispositivos externos na área de trabalho"

#: app/prefswindow.js:68 schemas/org.gnome.shell.extensions.ding.gschema.xml:50
#, fuzzy
msgid "Show network drives in the desktop"
msgstr "Mostrar dispositivos da rede na área de trabalho."

#: app/prefswindow.js:71
#, fuzzy
msgid "New icons alignment"
msgstr "Posição dos ícones novos"

#: app/prefswindow.js:73
msgid "Top-left corner"
msgstr "Canto superior esquerdo"

#: app/prefswindow.js:74
msgid "Top-right corner"
msgstr "Canto superior direito"

#: app/prefswindow.js:75
msgid "Bottom-left corner"
msgstr "Canto inferior esquerdo"

#: app/prefswindow.js:76
msgid "Bottom-right corner"
msgstr "Canto inferior direito"

#: app/prefswindow.js:78 schemas/org.gnome.shell.extensions.ding.gschema.xml:55
msgid "Add new drives to the opposite side of the screen"
msgstr "Mostrar novos dispositivos no lado oposto da tela"

#: app/prefswindow.js:79
msgid "Highlight the drop place during Drag'n'Drop"
msgstr "Destacar o local onde o ícone será alocado ao arrastar e soltar"

#: app/prefswindow.js:80 schemas/org.gnome.shell.extensions.ding.gschema.xml:90
msgid "Use Nemo to open folders"
msgstr ""

#: app/prefswindow.js:82
msgid "Add an emblem to soft links"
msgstr ""

#: app/prefswindow.js:84
msgid "Use dark text in icon labels"
msgstr ""

#: app/prefswindow.js:91
msgid "Settings shared with Nautilus"
msgstr "Configurações compartilhadas com Nautilus"

#: app/prefswindow.js:113
msgid "Click type for open files"
msgstr "Tipo de clique para abrir arquivos"

#: app/prefswindow.js:113
msgid "Single click"
msgstr "Clique único"

#: app/prefswindow.js:113
msgid "Double click"
msgstr "Clique duplo"

#: app/prefswindow.js:114
#, fuzzy
msgid "Show hidden files"
msgstr "Mostrar arquivos ocultos"

#: app/prefswindow.js:115
msgid "Show a context menu item to delete permanently"
msgstr "Mostrar a opção de excluir permanentemente no menu de contexto"

#: app/prefswindow.js:120
msgid "Action to do when launching a program from the desktop"
msgstr "O que fazer ao clicar num programa na área de trabalho"

#: app/prefswindow.js:121
msgid "Display the content of the file"
msgstr "Mostrar o conteúdo do arquivo"

#: app/prefswindow.js:122
msgid "Launch the file"
msgstr "Executar o arquivo"

#: app/prefswindow.js:123
msgid "Ask what to do"
msgstr "Perguntar o que fazer"

#: app/prefswindow.js:129
msgid "Show image thumbnails"
msgstr "Mostrar miniaturas"

#: app/prefswindow.js:130
msgid "Never"
msgstr "Nunca"

#: app/prefswindow.js:131
msgid "Local files only"
msgstr "Apenas arquivos locais"

#: app/prefswindow.js:132
msgid "Always"
msgstr "Sempre"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:25
msgid "Icon size"
msgstr "Tamanho do ícone"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:26
msgid "Set the size for the desktop icons."
msgstr "Define o tamanho para os ícones da área de trabalho."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:30
msgid "Show personal folder"
msgstr "Mostrar pasta pessoal"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:31
msgid "Show the personal folder in the desktop."
msgstr "Mostra a pasta pessoal na área de trabalho."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:35
msgid "Show trash icon"
msgstr "Mostrar ícone da lixeira"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:36
msgid "Show the trash icon in the desktop."
msgstr "Mostra o ícone da lixeira na área de trabalho."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:40
#, fuzzy
msgid "New icons start corner"
msgstr "Tamanho do ícone"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:41
msgid "Set the corner from where the icons will start to be placed."
msgstr "Escolha o canto da tela onde os ícones ficarão inicialmente"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:46
msgid "Show the disk drives connected to the computer."
msgstr "Mostrar as unidades de discos conectadas ao computador."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:51
#, fuzzy
msgid "Show mounted network volumes in the desktop."
msgstr "Mostra a pasta pessoal na área de trabalho."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:56
msgid ""
"When adding drives and volumes to the desktop, add them to the opposite side "
"of the screen."
msgstr ""
"Ao mostrar os discos e volumes montados, posicioná-los no lado oposto da "
"tela."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:60
msgid "Shows a rectangle in the destination place during DnD"
msgstr ""
"Mostrar um retângulo na pasta de destino ao arrastar e soltar um arquivo"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:61
msgid ""
"When doing a Drag'n'Drop operation, marks the place in the grid where the "
"icon will be put with a semitransparent rectangle."
msgstr ""
"Ao arrastar, destacar com um retângulo semitransparente o local onde o ícone "
"ficará alocado."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:65
msgid "Sort Special Folders - Home/Trash Drives."
msgstr "Ordenar pastas especiais - Home/Lixeira/Drives."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:66
msgid ""
"When arranging Icons on desktop, to sort and change the position of the "
"Home, Trash and mounted Network or External Drives"
msgstr ""
"Quando organizando ícones no desktop, ordene e modifique a posição de Home, "
"LIxeira e redes montadas ou Drives externos"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:70
msgid "Keep Icons Arranged"
msgstr "Manter Ícones Ordenados"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:71
msgid "Always keep Icons Arranged by the last arranged order"
msgstr ""
"Sempre mantenha ícone ordenados de acordo com a última forma de organizar"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:75
msgid "Arrange Order"
msgstr "Organizar ordem"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:76
msgid "Icons Arranged by this property"
msgstr "Ícones ordenados por esta propriedade"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:80
#, fuzzy
msgid "Keep Icons Stacked"
msgstr "Manter Ícones Ordenados"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:81
#, fuzzy
msgid "Always keep Icons Stacked, Similar types are grouped"
msgstr ""
"Sempre mantenha ícone ordenados de acordo com a última forma de organizar"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:85
msgid "Type of Files to not Stack"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:86
msgid "An Array of strings types, Don't Stack these types of files"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:91
msgid "Use Nemo instead of Nautilus to open folders."
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:95
msgid "Add an emblem to links"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:96
msgid "Add an emblem to allow to identify soft links."
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:100
msgid "Use black for label text"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:101
msgid ""
"Paint the label text in black instead of white. Useful when using light "
"backgrounds."
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:105
msgid "Show a popup if running on X11Wayland"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:106
msgid ""
"Whether DING should show a popup if it is running on X11Wayland, or the user "
"decided to not show it anymore."
msgstr ""

#, fuzzy
#~ msgid ""
#~ "To configure Desktop Icons NG, do right-click in the desktop and choose "
#~ "the last item: 'Desktop Icons settings'"
#~ msgstr ""
#~ "Para configurar Desktop Icons NG clique com o botão direito do mouse na "
#~ "área de trabalho e clique na última opção: 'Configurações'"

#~ msgid "Do you want to run “{0}”, or display its contents?"
#~ msgstr "Você deseja executar “{0}” ou exibir seu conteúdo?"

#~ msgid "“{0}” is an executable text file."
#~ msgstr "“{0}” é um arquivo executável."

#, fuzzy
#~ msgid "Execute in a terminal"
#~ msgstr "Abrir no terminal"

#~ msgid "Show"
#~ msgstr "Exibir"

#~ msgid "Execute"
#~ msgstr "Executar"

#, fuzzy
#~ msgid "New folder"
#~ msgstr "Nova pasta"

#~ msgid "Delete"
#~ msgstr "Excluir"

#~ msgid "Are you sure you want to permanently delete these items?"
#~ msgstr "Você tem certeza que deseja excluir permanentemente esses arquivos?"

#~ msgid "If you delete an item, it will be permanently lost."
#~ msgstr "Se você excluir um item, ele será perdido permanentemente."

#, fuzzy
#~ msgid "Show external disk drives in the desktop"
#~ msgstr "Mostrar dispositivos externos na área de trabalho"

#, fuzzy
#~ msgid "Show the external drives"
#~ msgstr "Mostrar dispositivos externos"

#, fuzzy
#~ msgid "Show network volumes"
#~ msgstr "Mostrar dispositivos da rede"

#~ msgid "Enter file name…"
#~ msgstr "Insira um nome de arquivo…"

#~ msgid "Folder names cannot contain “/”."
#~ msgstr "Nomes de pastas não podem conter “/”."

#~ msgid "A folder cannot be called “.”."
#~ msgstr "Uma pasta não pode ser chamada “.”."

#~ msgid "A folder cannot be called “..”."
#~ msgstr "Uma pasta não pode ser chamada “..”."

#~ msgid "Folders with “.” at the beginning of their name are hidden."
#~ msgstr "Pastas com “.” no começo de seus nomes são ocultas."

#~ msgid "There is already a file or folder with that name."
#~ msgstr "Já existe um arquivo ou uma pasta com esse nome."

#~ msgid "Huge"
#~ msgstr "Enorme"

#~ msgid "Ok"
#~ msgstr "Ok"
